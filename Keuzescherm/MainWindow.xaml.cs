﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Keuzescherm
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string file;
        private string window;

        public MainWindow()
        {
            InitializeComponent();
            EnableEllements(false);
            startGameButton.IsEnabled = false;
        }

        private void EnableEllements(bool enable)
        {
            koetjeMelkenRadioButton.IsEnabled = enable;
            hogerLagerRadioButton.IsEnabled = enable;
            pokerRadioButton.IsEnabled = enable;
        }

        private void exitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void startGameButton_Click(object sender, RoutedEventArgs e)
        {
            //hoe initialiseren we game? Om niet telkens dezelfde code te voozien?
            if (window == "Koetje Melken")
            {
                KoetjeMelken.MainWindow game = new KoetjeMelken.MainWindow(file);
                game.Show();
                game.Closed += WindowClosed;
            }
            else if (window == "Hoger-Lager")
            {
                HogerLager.MainWindow game = new HogerLager.MainWindow(file);
                game.Show();
                game.Closed += WindowClosed;
            }
            else
            {
                //code om poker aan te maken.
            }
            this.Hide();
        }

        private void openMenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog openFile = new OpenFileDialog();
                string startDirectory = Directory.GetParent(Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).FullName).FullName).FullName;
                openFile.InitialDirectory = startDirectory;
                openFile.Filter = "Text Files|*.txt;";
                if (openFile.ShowDialog() == true)
                {
                    file = openFile.FileName;
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Error: File not found: " + file + ". Retry.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error concerning file: " + file + ". " + ex.Message);
            }
            EnableEllements(true);
        }

        private void radioButton_Checked(object sender, RoutedEventArgs e)
        {
            startGameButton.IsEnabled = true;
            window = ((RadioButton)sender).Content.ToString();
        }

        private void WindowClosed(object sender, EventArgs e)
        {
            this.Show();
        }
    }
}
