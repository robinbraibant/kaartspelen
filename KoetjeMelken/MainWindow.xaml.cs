﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DeckOfCards;

namespace KoetjeMelken
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int valueComputer;
        int valuePlayer;
        int pointsComputer;
        int pointsPlayer;
        string file;

        Cards computer;
        Cards player;

        DispatcherTimer timer = new DispatcherTimer();
        DispatcherTimer endGameTimer = new DispatcherTimer();

        public MainWindow(string file)
        {
            InitializeComponent();
            this.file = file;

            dealCardButton.IsEnabled = false;
            playerStackCanvas.IsEnabled = false;
            resultTextBox.IsReadOnly = true;

            timer.Interval = TimeSpan.FromSeconds(2);
            timer.Tick += Timer_Tick;

            endGameTimer.Interval = TimeSpan.FromSeconds(2);
            endGameTimer.Tick += EndGameTimer_Tick;

            StartGame();
        }

        private void StartGame()
        {
            dealCardButton.IsEnabled = false;
            playerStackCanvas.IsEnabled = false;

            computer = new Cards(file);
            player = new Cards(file);

            shuffleButton.IsEnabled = true;
        }

        private void EndGameTimer_Tick(object sender, EventArgs e)
        {
            ClearComputer();
            ClearPlayer();

            string result;
            if (pointsComputer != pointsPlayer)
            {
                if (pointsComputer > pointsPlayer)
                {
                    result = "Sorry you lost.";
                }
                else
                {
                    result = "Congratulations!! You are the winner!!";
                }
                result += "The computer won " + pointsComputer + " times and you won " + pointsPlayer + " times";
            }
            else
            {
                result = "No winner. You have the same result as the computer: You won " + pointsPlayer + " times.";
            }
            resultTextBox.Text = result;
            playerStackCanvas.IsEnabled = false;

            string noCards = "NO MORE CARDS TO DEAL";
            string reshuffle = "Click shuffle cards to continue";
            computerCardNameTextBlock.Text = noCards;
            playerCardNameTextBlock.Text = noCards;

            computerCardnumberTextblock.Text = reshuffle;
            playerCardnumberTextblock.Text = reshuffle;
            endGameTimer.Stop();
        }

        private void ShuffleButton_Click(object sender, RoutedEventArgs e)
        {
            ClearTextBlocks();
            ClearComputer();
            ClearPlayer();
            computer.Shuffle();
            player.Shuffle();

            pointsComputer = 0;
            pointsPlayer = 0;

            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(computer.NextCardBackImage, UriKind.RelativeOrAbsolute);
            bi.EndInit();

            Image computerStackImage = new Image
            {
                Source = bi,
                Margin = new Thickness(0, 0, 0, 0),
                Width = playerDrawStackCanvas.Width,
                Height = playerDrawStackCanvas.Height
            };

            computerStackCanvas.Children.Add(computerStackImage);

            BitmapImage bi1 = new BitmapImage();
            bi1.BeginInit();
            bi1.UriSource = new Uri(player.NextCardBackImage, UriKind.RelativeOrAbsolute);
            bi1.EndInit();

            Image playerStackImage = new Image
            {
                Source = bi1,
                Margin = new Thickness(0, 0, 0, 0),
                Width = playerDrawStackCanvas.Width,
                Height = playerDrawStackCanvas.Height
            };

            playerStackCanvas.Children.Add(playerStackImage);

            shuffleCardsTextBlock.Text = "Decks are shuffled";

            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            DealComputer();
        }

        private void DealComputer()
        {
            compareComputerLabel.Content = "";
            comparePlayerLabel.Content = "";
            computer.NextCard();
            UpdateComputer();
            valueComputer = computer.CurrendCardActualValue;
            timer.Stop();
            dealCardButton.IsEnabled = true;
            playerStackCanvas.IsEnabled = true;
        }

        private void DealPlayer()
        {
            player.NextCard();
            UpdatePlayer();
            valuePlayer = player.CurrendCardActualValue;
            CompareValues();
            if (!player.IsLastCard())
            {
                timer.Start();
            }
            else
            {
                endGameTimer.Start();
            }
        }

        private void UpdateComputer()
        {
            ClearComputer();
            try
            {
                if (!computer.IsLastCard())
                {
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.UriSource = new Uri(computer.NextCardBackImage, UriKind.RelativeOrAbsolute);
                    bi.EndInit();

                    Image computerStackImage = new Image
                    {
                        Source = bi,
                        Margin = new Thickness(0, 0, 0, 0),
                        Width = computerStackCanvas.Width,
                        Height = computerStackCanvas.Height
                    };

                    computerStackCanvas.Children.Add(computerStackImage);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            BitmapImage bi1 = new BitmapImage();
            bi1.BeginInit();
            bi1.UriSource = new Uri(computer.CurrendCardImage, UriKind.RelativeOrAbsolute);
            bi1.EndInit();

            Image computerDrawImage = new Image
            {
                Source = bi1,
                Margin = new Thickness(0, 0, 0, 0),
                Width = computerDrawStackCanvas.Width,
                Height = computerDrawStackCanvas.Height
            };

            computerDrawStackCanvas.Children.Add(computerDrawImage);

            computerCardnumberTextblock.Text = computer.CardNumber;

            computerCardNameTextBlock.Text = computer.CurrendCardName;
        }

        private void ClearTextBlocks()
        {
            resultTextBox.Clear();
            computerCardNameTextBlock.Text = "";
            playerCardNameTextBlock.Text = "";
            computerCardnumberTextblock.Text = "";
            playerCardnumberTextblock.Text = "";
        }

        private void ClearComputer()
        {
            compareComputerLabel.Content = "";
            computerStackCanvas.Children.Clear();
            computerDrawStackCanvas.Children.Clear();
        }

        private void ClearPlayer()
        {
            comparePlayerLabel.Content = "";
            playerStackCanvas.Children.Clear();
            playerDrawStackCanvas.Children.Clear();
        }

        private void UpdatePlayer()
        {
            ClearPlayer();
            if (!player.IsLastCard())
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(player.NextCardBackImage, UriKind.RelativeOrAbsolute);
                bi.EndInit();

                Image playerStackImage = new Image
                {
                    Source = bi,
                    Margin = new Thickness(0, 0, 0, 0),
                    Width = playerStackCanvas.Width,
                    Height = playerStackCanvas.Height
                };

                playerStackCanvas.Children.Add(playerStackImage);
            }

            BitmapImage bi1 = new BitmapImage();
            bi1.BeginInit();
            bi1.UriSource = new Uri(player.CurrendCardImage, UriKind.RelativeOrAbsolute);
            bi1.EndInit();

            Image playerDrawImage = new Image
            {
                Source = bi1,
                Margin = new Thickness(0, 0, 0, 0),
                Width = playerDrawStackCanvas.Width,
                Height = playerDrawStackCanvas.Height
            };

            playerDrawStackCanvas.Children.Add(playerDrawImage);
            playerCardnumberTextblock.Text = player.CardNumber;
            playerCardNameTextBlock.Text = player.CurrendCardName;
        }

        private void DealCard_Click(object sender, MouseButtonEventArgs e)
        {
            DealPlayer();
            dealCardButton.IsEnabled = false;
            playerStackCanvas.IsEnabled = false;
        }

        private void DealCard_Click(object sender, RoutedEventArgs e)
        {
            DealPlayer();
            dealCardButton.IsEnabled = false;
            playerStackCanvas.IsEnabled = false;
        }

        private void CompareValues()
        {

            if (valueComputer > valuePlayer)
            {
                compareComputerLabel.Content = "Computer wins";
                pointsComputer++;
            }
            else if (valuePlayer > valueComputer)
            {
                comparePlayerLabel.Content = "You win";
                pointsPlayer++;
            }
            else
            {
                comparePlayerLabel.Content = "Equal";
                comparePlayerLabel.Content = "Equal";
            }
        }
    }
}


