﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HogerLager
{
    public class CardIndexOutOfBound : ApplicationException
    {
        public CardIndexOutOfBound(string message) : base("Index Out Of Bound: " + message + ". Kies een aantal tussen 2 en 51")
        {
        }
    }
}