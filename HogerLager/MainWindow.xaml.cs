﻿using DeckOfCards;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HogerLager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
	{
    	Cards spel;
    	string file;
        int aantalKaarten;
        string startCard;

    	public MainWindow(string file)
    	{
        	InitializeComponent();
        	this.file = file;
        	waardeSlider.ValueChanged += WaardeSlider_ValueChanged;
        	spel = new Cards(file);
            for (int i = 0; i < spel.Suits.Count; i++)
            {
                soortComboBox.Items.Add(spel.Suits[i]);
            }
            soortComboBox.SelectedIndex = 0;
    	}

    	private void WaardeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    	{
        	string suit = soortComboBox.SelectedValue.ToString();
            string value = spel.GetValue((int)(waardeSlider.Value -1));
            startCard = value + " of " + suit;

            BitmapImage bi = new BitmapImage();
        	bi.BeginInit();
        	bi.UriSource = new Uri(spel.GetCardImage(startCard), UriKind.RelativeOrAbsolute);
        	bi.EndInit();

        	Image playerDrawImage = new Image
        	{
            	Source = bi,
            	Margin = new Thickness(0, 0, 0, 0),
            	Width = cardCanvas.Width,
            	Height = cardCanvas.Height
        	};

        	cardCanvas.Children.Add(playerDrawImage);
    	}

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                aantalKaarten = Convert.ToInt32(aantalTextBox.Text);
                if (aantalKaarten > 51 || aantalKaarten < 2)
                {
                    throw new CardIndexOutOfBound(aantalKaarten.ToString());
                }
                HogerLagerGame game = new HogerLagerGame(aantalKaarten, startCard, file);
                game.Show();
            }
            catch (CardIndexOutOfBound ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}


