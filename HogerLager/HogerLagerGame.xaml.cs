﻿using DeckOfCards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HogerLager
{
    /// <summary>
    /// Interaction logic for HogerLagerGame.xaml
    /// </summary>
    public partial class HogerLagerGame : Window
    {
        private Canvas[] canvassen;
        private double leftMargin = 30;
        private double topMargin = 30;
        private Cards cards;
        private Card[] playCards;
        private Card startKaart;
        private int cardNumber = 0;

        public HogerLagerGame(int aantalKaarten, string startKaart, string file)
        {
            InitializeComponent();
            this.cards = new Cards(file);
            this.startKaart = cards.GetCard(startKaart);
            playCards = new Card[aantalKaarten];
            GenerateCanvases(aantalKaarten);
        }

        private void FillCanvases(int aantal)
        {
            GetPlayCards(aantal);

            FillCanvas(canvassen[0], playCards[0].Image);

            for (int i =1; i< canvassen.Length; i++)
            {
                FillCanvas(canvassen[i], playCards[i-1].BackImage);
            }

            FillCanvas(upButton, Environment.CurrentDirectory + @"\..\..\..\HogerLager\up.png");
            FillCanvas(downButton, Environment.CurrentDirectory + @"\..\..\..\HogerLager\down.png");
        }

        private void GetPlayCards(int aantal)
        {
            playCards[0] = startKaart;
            cards.RemoveCard(startKaart.Name);
            for (int i=1; i< aantal; i++)
            {
                Card currentCard = cards.GetRandomCard();
                playCards[i] = currentCard;
                cards.RemoveCard(currentCard.Name);                         
            }
        }

        private void FillCanvas(Canvas canvas, string pad)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(pad, UriKind.RelativeOrAbsolute);
            bi.EndInit();

            Image cardImage = new Image
            {
                Source = bi,
                Margin = new Thickness(0, 0, 0, 0),
                Width = canvas.Width,
                Height = canvas.Height
            };
            canvas.Children.Add(cardImage);
        }

        private void ClearCanvas(Canvas canvas)
        {
            canvas.Children.Clear();
        }

        private void GenerateCanvases(int aantal)
        {
            canvassen = new Canvas[aantal];
            for(int i = 0; i< canvassen.Length; i++)
            {
                canvassen[i] = new Canvas();
                Canvas currentCanvas = canvassen[i];
                currentCanvas.Width = 100;
                currentCanvas.Height = 125;
                currentCanvas.HorizontalAlignment = HorizontalAlignment.Left;
                currentCanvas.VerticalAlignment = VerticalAlignment.Top;
                double grid = this.Width;
                //double afstand = this.Width / aantal;
                double afstand = leftMargin + i * (grid - (2 * leftMargin)) / aantal;
                currentCanvas.Margin = new Thickness(afstand,topMargin,0,0);
                currentCanvas.Background = new SolidColorBrush(Colors.LightBlue);

                gameGrid.Children.Add(currentCanvas);
            }
            FillCanvases(aantal);
        }

        private void upButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            cardNumber++;
            ClearCanvas(canvassen[cardNumber]);
            FillCanvas(canvassen[cardNumber], playCards[cardNumber].Image);

            if(playCards[cardNumber-1].ActualValue > playCards[cardNumber].ActualValue)
            {
                MessageBox.Show("Jammer, volgende keer beter", "Hoger-Lager", MessageBoxButton.OK,MessageBoxImage.Warning);
                this.Close();
            }
            else if(cardNumber + 1 == playCards.Count())
            {
                MessageBox.Show("Proficiat, U heeft gewonen", "Hoger-Lager");
                this.Close();
            }
        }
        // todo spel herstart fout
        private void downButton_MouseUp(object sender, MouseButtonEventArgs e)
        {
            cardNumber++;
            ClearCanvas(canvassen[cardNumber]);
            FillCanvas(canvassen[cardNumber], playCards[cardNumber].Image);

            if (playCards[cardNumber - 1].ActualValue < playCards[cardNumber].ActualValue)
            {
                MessageBox.Show("Jammer, volgende keer beter", "Hoger-Lager", MessageBoxButton.OK, MessageBoxImage.Warning);
                this.Close();
            }
            else if (cardNumber + 1 == playCards.Count())
            {
                MessageBox.Show("Proficiat, U heeft gewonen", "Hoger-Lager");
                this.Close();
            }
        }
    }
}
