﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace DeckOfCards
{
    public class Cards
    {

        private List<Card> cards = new List<Card>();
        private int index = -1;
        private List<string> suits;
        private List<string> values;

        private static Random randomNumberGenerator = new Random();

        public Cards(string file)
        {
            ReadFile(file);

            foreach (string suit in suits)
            {
                int teller = 2;
                foreach (string value in values)
                {
                    cards.Add(new Card(value, suit, teller, Environment.CurrentDirectory + "\\..\\..\\..\\DeckOfCards\\cards\\" + value + suit + ".png"));
                    teller++;
                }
            }

        }

        public Card GetCard(string name)
        {
            foreach (Card currentCard in cards)
            {
                if (currentCard.Name == name)
                {
                    return currentCard;
                }
            }
            throw new CardNotFoundException(name);
        }

        public void RemoveCard(string cardName)
        {
            Card card = GetCard(cardName);
            cards.Remove(card);
        }

        public Card GetRandomCard()
        {
            return cards[randomNumberGenerator.Next(cards.Count - 1)];
        }

        public string GetCardImage(string card)
        {
            foreach(Card currentCard in cards)
            {
                if (currentCard.Name == card)
                {
                    return currentCard.Image;
                } 
            }
            throw new CardNotFoundException(card);
        }

        private void ReadFile(string file)
        {
            StreamReader inputStream = null;

            inputStream = File.OpenText(file);
            char separator = ',';
            string line = inputStream.ReadLine();
            values = (line.Split(separator)).ToList();
            line = inputStream.ReadLine();
            suits = (line.Split(separator)).ToList();

            if (inputStream != null)
            {
                inputStream.Close();
            }
        }
        public string GetValue(int number)
        {
            return values[number];
        }

        public List<string> Suits
        {
            get { return suits;}
        }


        public void Shuffle()
        {
            index = -1;
            int n = cards.Count;
            while (n > 1)
            {
                n--;
                int k = randomNumberGenerator.Next(n + 1);
                Card card = cards[k];
                cards[k] = cards[n];
                cards[n] = card;
            }
        }

        public int CurrendCardActualValue
        {
            get
            {
                return cards[index].ActualValue;
            }
        }

        public string CurrendCardName
        {
            get
            {
                return cards[index].Name;
            }
        }

        public string CurrendCardImage
        {
            get
            {
                return cards[index].Image;
            }
        }

        public string NextCardBackImage
        {
            get
            {
                return cards[index+1].BackImage;
            }
        }

        public string CardNumber
        {
            get
            {
                return "Cards #" + Convert.ToString(index + 1);
            }
        }

        public void NextCard()
        {
            if (cards.Count > 0)
            {
                index++;
            }
            else
            {
                throw new Exception("no cards in the deck.");
            }
        }

        public bool IsLastCard()
        {
            if (index == cards.Count-1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
