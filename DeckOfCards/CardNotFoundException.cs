﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeckOfCards
{
    public class CardNotFoundException : Exception
    {
        public CardNotFoundException(string message) : base("Card not found: " + message)
        {

        }
    }
}
