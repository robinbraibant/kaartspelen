﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeckOfCards
{
    public class Card
    {
        private string _value;
        private int actualValue;
        private string _image;
        private string _suit;
        private string _backImage;

        public Card(string value, string suit, int actualValue, string image)
        {
            this._value = value;
            this._image = image;
            this._suit = suit;
            this.actualValue = actualValue;
            _backImage = Environment.CurrentDirectory + "\\..\\..\\..\\DeckOfCards\\cards\\cardback.png";
            //_backImage = Directory.GetParent(Directory.GetParent(Directory.GetParent(Environment.CurrentDirectory).FullName).FullName).FullName + "\\DeckOfCards\\cards\\cardback.png";
            //_backImage = "../../cards/cardback.png";
        }

        public Card(string value, string suit, int actualValue, string image, string backImage)
        {
            this._value = value;
            this._image = image;
            this._suit = suit;
            this.actualValue = actualValue;
            this._backImage = backImage;
        }

        public int ActualValue
        {
            get
            {
                return actualValue;
            }
        }

        public string Name
        {
            get { return _value + " of " +  _suit; }
        }

        public string Value
        {
            get { return _value; }
        }

        public string Image
        {
            get { return _image; }
        }

        public string Suit
        {
            get { return _suit; }
        }

        public string BackImage
        {
            get { return _backImage; }
        }
    }
}
